<div class="body">
    <div class="row mt-4">
        <div class="col-md-12">
            <h2 class="text-center">Get in touch</h2>
            <h3 class="text-center">Feel free to ask us a question, don't be shy =)</h3>
        </div>
    </div>

    <form class="row mt-5 mb-5">
        <div class="form-group col-md-6 offset-md-3">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email address" required/>
        </div>

        <div class="form-group col-md-6 offset-md-3">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" id="subject" placeholder="Subject" required/>
        </div>

        <div class="form-group col-md-6 offset-md-3">
            <label for="message">Message</label>
            <textarea class="form-control" id="message" placeholder="Your message" rows="6" required></textarea>
        </div>

        <div class="form-group col-md-6 offset-md-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" name="copy" id="copy">
                <label class="form-check-label" for="copy">
                    I want to receive a copy of this email!
                </label>
            </div>
        </div>

        <div class="form-group col-md-6 offset-md-3">
            <button type="submit" title="The Acquired Taste - Send contact email!" class="btn btn-primary">Sign in</button>
        </div>
    </form>
</div>