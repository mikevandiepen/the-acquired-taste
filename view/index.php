</div>
<div class="body landing-wallpaper">
    <div class="container">

        <div class="row">
            <div class="col-md-12 header-color-white">
                <h1>Delivering THE whiskey experience to your doorstep!</h1>
            </div>
        </div>

        <div class="landing-info col-md-6 mt-5 text-center">
            <span class="card-title">Sign up now!</span><br>
            <span class="card-description">Get yourself a custom package delivered each month in three steps!</span><br>

            <div class="clearfix mb-5"></div>

            <a href="/subscribe" title="The Acquired Taste - Customize your bundle and subscribe to our service!" class="select-package-btn btn">
                Get started in three simple steps!
            </a>

            <div class="clearfix mt-3"></div>
        </div>
    </div>
</div>

<div class="features">
    <div class="container">
        <div class="row">
            <div class="col-md-4 feature-item">
                <i class="fas fa-glass-whiskey"></i>
                <h3>
                    A custom package
                </h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
            <div class="col-md-4 feature-item">
                <i class="fas fa-money-check"></i>
                <h3>
                    Affordable
                </h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
            <div class="col-md-4 feature-item">
                <i class="far fa-gem"></i>
                <h3>
                    High quality
                </h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="product-explanation-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">What is inside the box?</h2>
                <h5 class="text-center">It's really incredible!!</h5>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-6">
                <h4>Contents</h4>
                <hr>
                <ul>
                    <li class="head">Three small samples</li>
                    <li class="desc">Get three small samples to discover more brands and bottles!</li>
                    <li class="head">One large bottle picked by you</li>
                    <li class="desc">You can customize and choose a new bottle each month!</li>
                    <li class="head">Whisky bites</li>
                    <li class="desc">We handpick a complimentary snack for the best experience!</li>
                    <li class="head">Our monthly journal!</li>
                    <li class="desc">You'll receive our monthly journal filled with news about whisky and other things which you would love!</li>
                </ul>
            </div>
            <div class="col-md-6 justify-content-center text-center">
                <h4 class="mt-5">Already convinced?</h4>
                <div class="clearfix mb-5 mt-5"></div>
                <a href="/subscribe" title="The Acquired Taste - Customize your bundle and subscribe to our service!" class="select-package-btn btn">
                    Yes! Lets configure my package!
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container product-explanation-section">
    <div class="row">

        <hr>

        <div class="col-md-12">
            <h2 class="text-center">How does it work?</h2>
            <h5 class="text-center">It is really easy!</h5>

            <div class="clearfix mt-5"></div>

            <p class="text-center">
                With these three easy steps you can set yourself up to receive a personalized whisky box with 1 large bottle and three samples at the start of next month!
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 feature-item-steps">
            <i class="fas fa-sliders-h"></i>
            <h3>
                Customize your package
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
        </div>
        <div class="col-md-3 feature-item-steps">
            <i class="fas fa-money-check"></i>
            <h3>
                Checkout
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
        </div>
        <div class="col-md-3 feature-item-steps">
            <i class="fas fa-box-open"></i>
            <h3>
                Open the package
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
        </div>
        <div class="col-md-3 feature-item-steps">
            <i class="fas fa-glass-whiskey"></i>
            <h3>
                Adore the taste
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
        </div>
    </div>
</div>