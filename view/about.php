<div class="body">
    <div class="row mt-4">
        <div class="col-md-12">
            <h2 class="text-center">About us</h2>
            <h3 class="text-center">Learn about our history, the founder of the company and how we see the future of this company!</h3>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <h3>Our story</h3>
        </div>
    </div>

    <div class="row mb-5">
        <div class="col-md-6">
            <p>
                A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
                I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.
                I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents.
                I should be incapable of drawing a single stroke at the present moment;
            </p>
        </div>

        <div class="col-md-6">
            <p>
                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                Separated they live in Bookmarksgrove right at the coast of the
            </p>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <h4>Our vision</h4>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-7">
            <p>
                One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?" he thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. Drops
            </p>
        </div>

        <div class="col-md-5 mt-5">
            <blockquote class="blockquote">
                “Well, I wish some of you would tell me the brand of whiskey that Grant drinks. I would like to send a barrel of it to my other generals.” <br>
                <strong>― Abraham Lincoln </strong>
            </blockquote>
        </div>
    </div>
</div>