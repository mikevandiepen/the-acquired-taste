<div class="body">
    <div class="row mb-5">
        <div class="col-md-4">
            <h2 class="error-code">404</h2>
            <h3 class="error-title">Page not found!</h3>
        </div>
        <div class="col-md-8 mt-5">
            <h4 class="error-desc">
                Sorry, this page you're trying to reach does not exist (anymore).
            </h4>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-8 offset-2 mt-5">
            <h4 class="text-center">
                Questions?
            </h4>
        </div>
        <div class="col-md-8 offset-2">
            <h5 class="text-center">
                You can always get in touch with us if you have any questions regarding this error message!
            </h5>
        </div>
        <div class="col-md-8 offset-2 mt-4 text-center">
            <a href="/contact" title="The Acquired Taste - Contact us!" class="btn btn-primary">
                Get in touch!
            </a>
        </div>
    </div>
</div>
