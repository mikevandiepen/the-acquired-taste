<div class="container">
    <div class="product-explanation-section">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Customize your box!</h2>
                <h5 class="text-center">Use the customizer to create the perfect box for you!</h5>
            </div>
        </div>

        <div class="row mt-4">
            <form class="needs-validation mt-5 col-md-8 offset-md-2" novalidate>
                <div class="form-group">
                    <label for="age">Date of birth</label>
                    <input type="date" name="age" class="form-control" id="age" placeholder="your age" required/>
                </div>

                <label for="min-price" class="float-left">Minimum price you want to pay per cycle</label>
                <label for="max-price" class="float-right">Maximum price you want to pay per cycle</label>

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                        <span class="input-group-text">0.00</span>
                    </div>
                    <input type="text" name="min-price" class="form-control" id="min-price" aria-label="Amount (to the nearest dollar)"/>

                    <input type="text" name="max-price"  class="form-control" id="max-price" aria-label="Amount (to the nearest dollar)"/>
                    <div class="input-group-append">
                        <span class="input-group-text">$</span>
                        <span class="input-group-text">0.00</span>
                    </div>
                </div>

                <div class="form-group mb-5 mt-2">
                    <label for="select-bottle-lg">Select your large bottle (70cl)</label>
                    <select name="select-bottle-lg" class="custom-select" id="select-bottle-lg" required>
                        <optgroup label="Jack Daniel's">
                            <option value="Tennessee Whiskey">Tennessee Whiskey</option>
                            <option value="Old No. 7 Black Label">Old No. 7 Black Label</option>
                            <option value="Gentleman Jack">Gentleman Jack</option>
                            <option value="Rye Whiskey">Rye Whiskey</option>
                            <option value=" Single Barrel Select"> Single Barrel Select</option>
                        </optgroup>
                        <optgroup label="Johnnie Walker">
                            <option value="Red Label">Red Label</option>
                            <option value="Black Label">Black Label</option>
                            <option value="Double Black">Double Black</option>
                            <option value="Gold Label Reserve">Gold Label Reserve</option>
                            <option value="Platinum Label">Platinum Label</option>
                        </optgroup>
                        <optgroup label="Talisker">
                            <option value="10 Year old">10 Year old</option>
                            <option value="18 Year old">18 Year old</option>
                            <option value="57º North">57º North</option>
                            <option value="Storm">Storm</option>
                            <option value="2000 Distillers Edition">2000 Distillers Edition</option>
                        </optgroup>
                        <optgroup label="Glenfiddich">
                            <option value="15 Year Old Solera Reserve">15 Year Old Solera Reserve</option>
                            <option value="IPA Experiment">IPA Experiment</option>
                            <option value="18 Year old Small Batch Reserve">18 Year old Small Batch Reserve</option>
                            <option value="30 Year old">30 Year old</option>
                            <option value="15 Year old Whisky">15 Year old Whisky</option>
                        </optgroup>
                    </select>
                    <div class="invalid-feedback">Example invalid custom select feedback</div>
                </div>

                <h6>Shipping</h6>
                <hr>

                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">First name</label>
                        <input type="text" name="first_name" class="form-control" id="validationCustom01" placeholder="First name" value="" required/>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom02">Last name</label>
                        <input type="text" name="last_name" class="form-control" id="validationCustom02" placeholder="Last name" value="" required/>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustomUsername">Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                            </div>
                            <input type="text" class="form-control" id="validationCustomUsername" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationCustom03">City</label>
                        <input type="text" name="city" class="form-control" id="validationCustom03" placeholder="City" required/>
                        <div class="invalid-feedback">
                            Please provide a valid city.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom04">State</label>
                        <input type="text" name="state" class="form-control" id="validationCustom04" placeholder="State" required/>
                        <div class="invalid-feedback">
                            Please provide a valid state.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom05">Zip</label>
                        <input type="text" name="zip_code" class="form-control" id="validationCustom05" placeholder="Zip" required/>
                        <div class="invalid-feedback">
                            Please provide a valid zip.
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input name="terms_and_conditions" class="form-check-input" type="checkbox" value="" id="invalidCheck" required/>
                        <label class="form-check-label" for="invalidCheck">
                            Agree to terms and conditions
                        </label>
                        <div class="invalid-feedback">
                            You must agree before submitting.
                        </div>
                    </div>
                </div>
                <button name="submit" title="The Acquired Taste - Submit subscription request!" class="btn btn-primary" type="submit">Request subscription!</button>
            </form>
        </div>
    </div>
</div>