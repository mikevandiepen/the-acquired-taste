<!doctype html>
<html lang="en">
<head>

    <!-- Metadata -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- SEO -->
    <meta content="<?= $description ?>" name="description">
    <meta content="<?= implode(', ', $tags) ?>" name="keywords">

    <!-- OpenGraph content -->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="The Acquired taste">
    <meta property="og:url" content="<?= 'http://acquired-taste.mediadevs.nl/' . $_GET['page'] ?? ''?>">
    <meta property="og:image" content="http://acquired-taste.mediadevs.nl/src/images/wallpaper.jpeg">
    <meta property="og:title" content="<?= $title ?>">
    <meta property="og:description" content="<?= $description ?>">
    <meta property="og:locale" content="en_GB">
    <meta property="og:locale:alternate" content="en_US">
    <meta property="og:locale:alternate" content="en_IN">

    <!-- Twitter content -->
    <meta name="twitter:card" content="<?= $description ?>">
    <meta name="twitter:image" content="http://acquired-taste.mediadevs.nl/src/images/wallpaper.jpeg">
    <meta name="twitter:title" content="<?= $title ?>">
    <meta name="twitter:description" content="<?= $description ?>">
    <meta name="twitter:site" content="@the_acquired_taste">



    <!-- Assets -->
    <script src="../dist/bundle.js"></script>
    <link rel="stylesheet" href="../dist/bundle.css" type="text/css">

    <!-- Title -->
    <title><?= $title ?></title>

</head>
<body>
<!-- Start of content -->