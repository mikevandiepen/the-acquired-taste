</div>
<!-- End of page content -->
<!-- start of sub-footer -->
<div class="sub-footer">
    <div class="container">
        <div class="row">
            <ul>
                <li><img class="img" id="talisker"      alt="Logo of Talisker (A whiskey brand)"/></li>
                <li><img class="img" id="glenfiddich"   alt="Logo of Glenfiddich (A whiskey brand)"/></li>
                <li><img class="img" id="jackdaniels"   alt="Logo of jackdaniels (A whiskey brand)"/></li>
                <li><img class="img" id="johnniewalker" alt="Logo of Johnniewalker (A whiskey brand)"/></li>
            </ul>
        </div>
    </div>
</div>
<!-- end of sub-footer -->
<footer>
    <div class="container">
        <div class="row">
            <address class="col-md-4">
                Korenmolen 193<br>
                1622 JA Hoorn<br>
                Noord-Holland, Nederland<br>
                <a href="tel:+31636772232" title="The phone number of Acquired-Taste">+31 (0)6-36772232</a>
            </address>
            <nav class="footer-nav col-md-8">
                <div class="row">

                    <div class="col-md-6">
                        <ul>
                            <li>
                            <span class="footer-nav-title">
                               - Legal
                            </span>
                            </li>
                            <li><a href="" title="Read our Privacy Policy">Privacy Policy</a></li>
                            <li><a href="" title="Read our Cookie Policy">Cookie Policy</a></li>
                            <li><a href="" title="Read about our data retention">Data retention</a></li>
                            <li><a href="" title="The payment details">Payment details</a></li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <ul>
                            <li>
                            <span class="footer-nav-title">
                                - Social
                            </span>
                            </li>
                            <li><a href="" title="Visit our Facebook page">Facebook</a></li>
                            <li><a href="" title="Visit our twitter page">Twitter</a></li>
                            <li><a href="" title="Visit our LinkedIn page">LinkedIn</a></li>
                            <li><a href=""  title="Visit our Instagram page">Instagram</a></li>
                        </ul>
                    </div>

                </div>
            </nav>
        </div>
    </div>
</footer>
<!-- End of content -->
</body>
</html>