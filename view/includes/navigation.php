<div class="top-nav">
    <p>We've added 12 new brands to our assortment, click <a href="" title="The Acquired Taste - Check out our new brands!">here</a> to check it out!</p>
</div>
<nav class="header-nav">
    <div class="container">
        <div class="row">
            <h1>The Acquired Taste</h1>
        </div>
        <div class="row">
            <ul>
                <li>
                    <a href="/" title="The Acquired Taste - Visit our home page!">Home</a>
                </li>
                <li>
                    <a href="/about" title="The Acquired Taste - Learn about us!">About</a>
                </li>
                <li>
                    <a href="/contact" title="The Acquired Taste - Get in touch!">Contact</a>
                </li>
                <li>
                    <a href="/subscribe" title="The Acquired Taste - Create your own whiskey package!">Get started!</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- start of sub-header -->
<div class="sub-header">
    <div class="container">
        <div class="row">
            <ul>
                <li class="sub-header-item  col-md-3">
                    <address>
                        <span class="sub-header-icon">
                            <i class="fas fa-map-marked-alt"></i>
                        </span>
                        Korenmolen 193
                        1622 JA Hoorn
                    </address>
                </li>
                <li class="sub-header-item col-md-3">
                    <span class="sub-header-icon">
                        <i class="fas fa-truck-moving"></i>
                    </span>
                    Free shipping in The Netherlands
                </li>
                <li class="sub-header-item col-md-3">
                    <span class="sub-header-icon">
                        <i class="fas fa-calendar-alt"></i>
                    </span>
                    Custom package each month
                </li>
                <li class="sub-header-item col-md-3">
                    Rated
                    <span class="sub-header-icon">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </span>
                    by <span class="sub-header-label">1444</span>
                    <span class="sub-header-icon">
                        <i class="fas fa-user"></i>
                    </span>

                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end of sub-footer -->
<!-- Start of page content -->
<div class="container">
