## Setup
Om het project draaiend te krijgen moet je enkele commands runnen.

- `$ git clone https://mikevandiepen@bitbucket.org/mikevandiepen/the-acquired-taste.git`

- `$ cd ./path/to/local/to/the-acquired-taste/`

- `$ npm install`

- `$ yarn`

- `$ npm run dev`

#### Quick copy paste
- ```git clone https://mikevandiepen@bitbucket.org/mikevandiepen/the-acquired-taste.git && cd ./the-acquired-taste && npm install && yarn && npm run dev```

Na deze stappen zal het project up & running zijn, pak tijdens het installeren van de node packages gerust een bakje koffie!

#### Link naar mijn setup document
https://docs.google.com/document/d/1PN8MLhfRbOc4m-3aTMcnRuKNTNTir0KMwpntuHj2Mv4/edit?usp=sharing

#### Link naar live website
https://acquired-taste.mediadevs.nl/